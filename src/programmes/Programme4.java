package programmes;

import java.util.Scanner;

public class Programme4 {
   
  public static void main(String [] args){   
      
    int[] tableau=  { 45,23,33,8};
    
    int   indice;
    
    Scanner  clavier= new Scanner(System.in);
      
    System.out.println("Indice de l'élément à afficher? ");
    indice=clavier.nextInt();
        
    System.out.println("L'élément d'indice "+indice+" est "+ tableau[indice]);           
  
  }  
}



