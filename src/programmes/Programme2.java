package programmes;

public class Programme2 {
                
    public static void main(String [] args){   
       
       // Parcours et affichage des éléments du tableau sans utiliser d'indice 
       // Pour chaque élément  x de type entier(int) du tableau
    
        int[] tableau = { 45, 23, 33, 8 };
        
        for( int x : tableau ){
       
           System.out.println(x);
        }
    }    
}




