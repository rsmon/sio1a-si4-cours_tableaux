package programmes;
import java.util.Scanner;

public class Lanceur {
    
 public static void main(String[] args) {
   
   String itemMenu1 ="1) Affichage des éléments d'un tableau d'entiers version 1";
   String itemMenu2 ="2) Affichage des éléments d'un tableau d'entiers version 2";
   String itemMenu3 ="3) Affichage inverse des éléments d'un tableau d'entiers";
   String itemMenu4 ="4) Affichage d'un élément d'indice donné du tableau d'entiers";     
   String itemMenu5 ="5) Initialisation aléatoire d'un tableau d'entiers et affichage";
   String itemMenu6 ="6) tableau de chaîne de caractères, affichage et recherche";
        
   Scanner  clavier= new Scanner(System.in);
        
   int choix=0;
        
   do{
            
      System.out.println(itemMenu1);
      System.out.println(itemMenu2);
      System.out.println(itemMenu3);
      System.out.println(itemMenu4);
      System.out.println(itemMenu5);
      System.out.println(itemMenu6);
      System.out.println();
        
      System.out.print("Programme à lancer ( tapez 0 pour arrêter) ? "); 
      choix=clavier.nextInt();
    
      System.out.println();
            
      switch (choix)             
      { 
        case 1: Programme1.main(null);break;
        case 2: Programme2.main(null);break;
        case 3: Programme3.main(null);break;
        case 4: Programme4.main(null);break; 
        case 5: Programme5.main(null);break;
        case 6: Programme6.main(null);break;    
      }  
       
      System.out.println();
            
     }while ( choix > 0);
  
 }
}



