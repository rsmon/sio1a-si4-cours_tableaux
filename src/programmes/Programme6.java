package programmes;
import java.util.Scanner;

public class Programme6 {
    
    public static void main(String [] args){    
    
      Scanner  clavier = new Scanner(System.in);
    
      String[] tableau = { "Sophie","Alain","Jacques","Béatrice" };
    
      int    indiceRecherche;  // variable locale
        
      for( int indice=0; indice<tableau.length; indice++ ) {
       
         System.out.println("indice: "+indice+"  valeur: "+tableau[indice]);
      } 
       
      System.out.println("\n\n");
       
      System.out.println("Indiquez l'indice de l'élément recherché (entre 0 et 3)");
      indiceRecherche=clavier.nextInt(); 
             
      System.out.println("L'élément d'indice "+indiceRecherche+ " est "+tableau[indiceRecherche]);         
 }    
}






